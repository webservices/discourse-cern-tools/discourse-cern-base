FROM ubuntu:16.04

LABEL maintainer="web-services-core <web-services-core@cern.ch>" \
      io.openshift.expose-services="8080,9292:http"

ENV PG_MAJOR 11
ENV RUBY_ALLOCATOR /usr/lib/libjemalloc.so.1
ENV RAILS_ENV production

### Install prerequisites #############################
RUN apt update && apt install -y lsb-release curl
RUN echo "debconf debconf/frontend select Teletype" | debconf-set-selections
RUN echo "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main restricted universe" > /etc/apt/sources.list
RUN echo "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc)-updates main restricted universe" >> /etc/apt/sources.list
RUN echo "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc)-security main restricted universe" >> /etc/apt/sources.list

RUN apt-get -y install software-properties-common lsof vim gettext

RUN apt-get -y upgrade
RUN curl https://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add -
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main" | \
        tee /etc/apt/sources.list.d/postgres.list
RUN curl --silent --location https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get -y update
RUN apt-get -y install build-essential git wget \    
    libxslt-dev libcurl4-openssl-dev \
    libssl-dev libyaml-dev libtool \
    libxml2-dev gawk parallel \
    # Install postgres
    postgresql-client-${PG_MAJOR} postgresql-contrib-${PG_MAJOR} libpq-dev libreadline-dev \    
    language-pack-en \
    psmisc brotli libunwind-dev \
    # Efficient thread-caching malloc
    libtcmalloc-minimal4    
    
RUN cd / && \
    apt-get clean && \
    locale-gen en_US &&\
    apt-get install -y nodejs &&\
    npm install -g uglify-js@"<3" &&\
    npm install -g svgo

### Configure Nginx
ADD install-nginx /tmp/install-nginx
RUN chmod +x /tmp/install-nginx && /tmp/install-nginx

### N.B.: https://www.redpill-linpro.com/sysadvent/2017/12/10/jekyll-openshift.html
### /var/run is configured for different pids. Check unicorn.conf.rb and sidekiq.yml configurations.
RUN \
  ln -sf /dev/stdout /var/log/nginx/access.log && \
  ln -sf /dev/stderr /var/log/nginx/error.log && \
  mkdir -p /etc/nginx/ /var/run /var/cache/nginx /var/lib/nginx /var/log/nginx && \
  chgrp -R 0 /etc/nginx/ /var/run /var/cache/nginx /var/lib/nginx /var/log/nginx && \
  chmod -R g=u /etc/nginx/ /var/run /var/cache/nginx /var/lib/nginx /var/log/nginx

### Install jemalloc
RUN mkdir /jemalloc-stable && cd /jemalloc-stable &&\
    wget https://github.com/jemalloc/jemalloc/releases/download/3.6.0/jemalloc-3.6.0.tar.bz2 &&\
    tar -xjf jemalloc-3.6.0.tar.bz2 && cd jemalloc-3.6.0 && ./configure --prefix=/usr && make && make install &&\
    cd / && rm -rf /jemalloc-stable

RUN echo 'gem: --no-document' >> /usr/local/etc/gemrc &&\
    mkdir /src && cd /src && git clone https://github.com/sstephenson/ruby-build.git &&\
    cd /src/ruby-build && ./install.sh &&\
    cd / && rm -rf /src/ruby-build && (ruby-build 2.6.3 /usr/local)

RUN gem update --system

RUN gem install bundler --force &&\
    rm -rf /usr/local/share/ri/2.6.3/system &&\
    cd / && git clone https://github.com/discourse/pups.git

# Install image utilities
RUN apt-get -y install advancecomp jhead jpegoptim libjpeg-turbo-progs optipng && \
    apt-get clean

ADD install-imagemagick /tmp/install-imagemagick
RUN chmod +x /tmp/install-imagemagick && /tmp/install-imagemagick

# Validate install
RUN ruby -Eutf-8 -e "v = \`convert -version\`; %w{png tiff jpeg freetype}.each { |f| unless v.include?(f); STDERR.puts('no ' + f +  ' support in imagemagick'); exit(-1); end }"

ADD install-pngcrush /tmp/install-pngcrush
RUN chmod +x /tmp/install-pngcrush && /tmp/install-pngcrush

ADD install-gifsicle /tmp/install-gifsicle
RUN chmod +x /tmp/install-gifsicle && /tmp/install-gifsicle

ADD install-pngquant /tmp/install-pngquant
RUN chmod +x /tmp/install-pngquant && /tmp/install-pngquant

# clean up for docker squash
RUN   rm -fr /usr/share/man &&\
      rm -fr /usr/share/doc &&\
      rm -fr /usr/share/vim/vim74/tutor &&\
      rm -fr /usr/share/vim/vim74/doc &&\
      rm -fr /usr/share/vim/vim74/lang &&\
      rm -fr /usr/local/share/doc &&\
      rm -fr /usr/local/share/ruby-build &&\
      rm -fr /root/.gem &&\
      rm -fr /root/.npm &&\
      rm -fr /tmp/* &&\
      rm -fr /usr/share/vim/vim74/spell/en*